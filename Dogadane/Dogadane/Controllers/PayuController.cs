﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dogadane.Interfaces;
using Dogadane.Models.Payu;
using Dogadane.Services;
using Dogadane.Utils;

namespace Dogadane.Controllers
{
    public class PayuController : Controller
    {
        IOrderService _orderService = new OrderService(new OrderValidator());
        private string algorithm = "SHA-256";

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetSignature(OrderData orderData)
        {
            var signature = _orderService.CreateSignature(orderData, "cb83ef4311d9d8e7263dd5d2d4d2d617");
            var hashedOrderSignature = Helpers.CreateAndHashOrderSignature(signature, orderData.MerchantPosId, algorithm);

            return string.IsNullOrEmpty(hashedOrderSignature)
                ? Json("Invalid")
                : Json(hashedOrderSignature, JsonRequestBehavior.AllowGet);
        }
    }
}