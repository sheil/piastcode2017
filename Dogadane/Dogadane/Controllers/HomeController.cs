﻿using System.Linq;
using System.Web.Mvc;
using Dogadane.Models;
using Dogadane.ViewModels;

namespace Dogadane.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            using (var cont = new ApplicationDbContext())
            {
                var model = new SearchIndexViewModel
                {
                    CategoryOptions = cont.Categories.AsNoTracking().OrderBy(c => c.Name).ToList()
                };
                return View(model);
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}