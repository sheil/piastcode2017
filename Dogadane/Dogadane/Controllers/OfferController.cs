﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dogadane.Models;
using Dogadane.ViewModels;
using Dogadane.ViewModels.Enums;
using Microsoft.AspNet.Identity;

namespace Dogadane.Controllers
{
    public class OfferController : Controller
    {
        [HttpPost]
        public ActionResult Add(AddOfferViewModel vm)
        {
            try
            {
                using (var cont = new ApplicationDbContext())
                {
                    var dbOffer = new Offer
                    {
                        CategoryId = vm.CategoryId,
                        Title = vm.Title,
                        Description = vm.Description,
                        DateAdded = DateTime.Now,
                        SourceUserId = User.Identity.GetUserId(),
                        Status = OfferStatus.Open
                    };
                    cont.Offers.Add(dbOffer);
                    cont.SaveChanges();
                }
                return Json(new {Success = true});
            }
            catch (Exception)
            {
                return Json(new {Success = false});
            }

        }

        public ActionResult UserOffers()
        {
            using (var dbContext = new ApplicationDbContext())
            {
                var user = dbContext.Users.SingleOrDefault(u => u.UserName == User.Identity.Name);

                #region divide offers

                List<OfferItemViewModel> openOffersViewModels =
                    dbContext.Offers.Where(o => o.SourceUserId == user.Id && o.Status == OfferStatus.Open)
                        .Select(offer => new OfferItemViewModel
                        {
                            CategoryName = offer.Category.Name,
                            Description = offer.Description,
                            Title = offer.Title,
                            Id = offer.Id
                        }).ToList();

                List<OfferItemViewModel> bookedOffersViewModels =
                    dbContext.Offers.Where(o => o.SourceUserId == user.Id && o.Status == OfferStatus.Booked)
                        .Select(offer => new OfferItemViewModel
                        {
                            CategoryName = offer.Category.Name,
                            Description = offer.Description,
                            Title = offer.Title,
                            Id = offer.Id
                        }).ToList();

                List<OfferItemViewModel> closedOffersViewModels =
                    dbContext.Offers.Where(o => o.SourceUserId == user.Id && o.Status == OfferStatus.Closed)
                        .Select(offer => new OfferItemViewModel
                        {
                            CategoryName = offer.Category.Name,
                            Description = offer.Description,
                            Title = offer.Title,
                            Id = offer.Id
                        }).ToList();

                #endregion //hardcode xoxoxoxo hardcode xoxoxoxo

                UserOffersViewModel viewModel = new UserOffersViewModel
                {
                    BookedOffers = bookedOffersViewModels,
                    ClosedOffers = closedOffersViewModels,
                    OpenOffers = openOffersViewModels
                };

                return View(viewModel);
            }
        }

        public ActionResult Search(int categoryId, string input, int range)
        {
            using (var cont = new ApplicationDbContext())
            {
                if (string.IsNullOrWhiteSpace(input))
                    input = null;
                var offers =
                    cont.Offers.AsNoTracking()
                        .Where(
                            e =>
                                (categoryId == 0 || e.CategoryId == categoryId) &&
                                (input == null || e.Title.Contains(input)) && e.Status == OfferStatus.Open)
                        .Take(50)
                        .Select(e => new OfferItemViewModel
                        {
                            Id = e.Id,
                            CategoryName = e.Category.Name,
                            Description = e.Description,
                            Title = e.Title,
                            MainSkill = e.SourceUser.Skills.FirstOrDefault().Name
                        })
                        .ToList(); //TODO: range!
                return Json(offers, JsonRequestBehavior.AllowGet);
            }
        }



        public ActionResult GetCategories()
        {
            using (var cont = new ApplicationDbContext())
            {
                var categories = cont.Categories.AsNoTracking().OrderBy(c => c.Name).ToList();
                return Json(categories, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Rate(RateViewModel vm)
        {
            try
            {
                using (var cont = new ApplicationDbContext())
                {
                    var dbRate = new Rate
                    {
                        OfferId = vm.OfferId,
                        SourceUserId = User.Identity.GetUserId(),
                        TargetUserId = User.Identity.GetUserId(),
                        Contact = vm.Contact,
                        Reliability = vm.Reliability,
                        Skill = vm.Skill
                    };
                    cont.Rates.Add(dbRate);
                    cont.SaveChanges();
                }
                return Json(new {Success = true});
            }
            catch (Exception)
            {
                return Json(new {Success = false});
            }
        }

        [HttpPost]
        public ActionResult Book(int offerId)
        {
            try
            {
                using (var cont = new ApplicationDbContext())
                {
                    var offer = cont.Offers.SingleOrDefault(c => c.Id == offerId);
                    if (offer == null)
                        return Json(new {Success = false});
                    offer.AssociatedUserId = User.Identity.GetUserId();
                    offer.Status = OfferStatus.Booked;

                    cont.SaveChanges();
                    return Json(new {Success = true});
                }
            }
            catch (Exception)
            {
                return Json(new {Success = false});
            }
        }

        public ActionResult Details(int? offerId)
        {
            using (var cont = new ApplicationDbContext())
            {
                var offer =
                    cont.Offers.Include(e => e.SourceUser.Rates)
                        .Include(e => e.SourceUser.Skills)
                        .SingleOrDefault(c => c.Id == offerId);

                return View(offer);
            }
        }

        public ActionResult Close(int offerId)
        {
            try
            {
                using (var cont = new ApplicationDbContext())
                {
                    var offer = cont.Offers.SingleOrDefault(c => c.Id == offerId);
                    if (offer == null)
                        return Json(new { Success = false });
                    offer.Status = OfferStatus.Closed;

                    cont.SaveChanges();
                    return Json(new { Success = true });
                }
            }
            catch (Exception)
            {
                return Json(new {Success = false});
            }
        }
    }
}