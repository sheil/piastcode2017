﻿using System.Linq;
using Dogadane.Interfaces;
using Dogadane.Models.Payu;
using Dogadane.Utils;

namespace Dogadane.Services
{
    public class OrderValidator : IOrderValidator
    {
        public bool IsOrderDataValid(OrderData orderData)
        {
            var generalParametersValid = ValidateGeneralParameters(orderData);
            var buyerValid = ValidateBuyer(orderData);
            var productsValid = ValidateProducts(orderData);
            var shippingMethodsValid = ValidateShippingMethods(orderData);

            return generalParametersValid && buyerValid && productsValid &&
                   shippingMethodsValid;
        }

        public bool ValidateShippingMethods(OrderData orderData)
        {
            if (orderData.ShippingMethods == null)
                return true;

            var productsValid = true;

            orderData.ShippingMethods.ForEach(p =>
            {
                if (!AllValid(p.Name, p.Price, p.Country))
                    productsValid = false;
            });

            return productsValid;
        }

        public bool ValidateProducts(OrderData orderData)
        {
            if (orderData.Products == null)
                return false;

            var productsValid = true;

            orderData.Products.ForEach(p =>
            {
                if (!AllValid(p.Name, p.UnitPrice, p.Quantity))
                    productsValid = false;
            });

            return productsValid;
        }

        public bool ValidateBuyer(OrderData orderData)
        {
            return orderData.Buyer == null || AllValid(orderData.Buyer.Email, orderData.Buyer.FirstName, orderData.Buyer.LastName);
        }

        public bool ValidateDelivery(OrderData orderData)
        {
            if (orderData.Buyer != null)
            {
                return orderData.Buyer.Delivery == null ||
                    AllValid(orderData.Buyer?.Delivery.Street, orderData.Buyer?.Delivery.PostalCode,
                        orderData.Buyer?.Delivery.City, orderData.Buyer?.Delivery.CountryCode);
            }
            return true;
        }

        public bool ValidateInvoice(OrderData orderData)
        {
            if (orderData.Buyer != null)
            {
                return orderData.Buyer.Invoice == null ||
                    AllValid(orderData.Buyer?.Invoice.Street, orderData.Buyer?.Invoice.PostalCode,
                             orderData.Buyer?.Invoice.City, orderData.Buyer?.Invoice.CountryCode);
            }
            return true;
        }

        public bool ValidateGeneralParameters(OrderData orderData)
        {
            return AllValid(orderData.CustomerIp, orderData.MerchantPosId, orderData.Description,
                orderData.CurrencyCode, orderData.TotalAmount);
        }

        public bool AllValid(params object[] values)
        {
            return values.All(v => !Helpers.IsNullOrEmpty(v));
        }
    }
}
