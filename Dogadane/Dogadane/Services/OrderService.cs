﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using Dogadane.Interfaces;
using Dogadane.Models.Payu;
using Dogadane.Utils;

namespace Dogadane.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderValidator _validator;

        public OrderService(IOrderValidator validator)
        {
            _validator = validator;
        }

        public string CreateSignature(OrderData orderData, string clientKey = "cb83ef4311d9d8e7263dd5d2d4d2d617")
        {
            if (orderData == null)
                return string.Empty;

            if (!_validator.IsOrderDataValid(orderData))
                return string.Empty;

            var sortedFormProperties = GetFormProperties(orderData).OrderBy(p => p.Key).ToList();

            return Helpers.ReplaceUtf8LowercaseWithUppercase(Helpers.ConcatenateSignature(clientKey, sortedFormProperties));
        }

        public Dictionary<string, string> GetFormProperties(OrderData orderData)
        {
            if (orderData == null)
                return new Dictionary<string, string>();

            var formProperties = new Dictionary<string, string>();

            foreach (var pi in orderData.GetType().GetProperties())
            {
                GetOrderProperties(pi, orderData, formProperties);
                GetBuyerProperties(pi, orderData, formProperties);
                GetProductProperties(pi, orderData, formProperties);
                GetShippingMethodProperties(pi, orderData, formProperties);
            }
            return formProperties;
        }

        public void GetOrderProperties(PropertyInfo pi, OrderData orderData, Dictionary<string, string> formProperties)
        {
            if (orderData == null) return;
            if (pi.PropertyType != typeof(string)) return;

            var value = (string)pi.GetValue(orderData);

            if (!string.IsNullOrEmpty(value))
                formProperties.Add(pi.Name.FirstLetterToLower(), value);
        }

        public void GetBuyerProperties(PropertyInfo pi, OrderData orderData, Dictionary<string, string> formProperties)
        {
            if (orderData == null) return;
            if (pi.PropertyType != typeof(Buyer)) return;

            var buyer = (Buyer)pi.GetValue(orderData);
            if (buyer == null) return;

            foreach (var propertyInfo in buyer.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(Delivery))
                {
                    GetDeliveryProperties(propertyInfo, orderData, formProperties);
                }
                else if (propertyInfo.PropertyType == typeof(Invoice))
                {
                    GetInvoiceProperties(propertyInfo, orderData, formProperties);
                }
                else
                {
                    var buyerProperty = propertyInfo.GetValue(buyer);
                    AddToFormProperties(formProperties, buyerProperty, propertyInfo, "buyer");
                }
            }
        }

        private void AddToFormProperties(Dictionary<string, string> formProperties, object property,
            PropertyInfo propertyInfo, string keyStart)
        {
            if (property == null) return;

            if (!Helpers.IsNullOrEmpty(property))
                formProperties.Add($"{keyStart}.{propertyInfo.Name.FirstLetterToLower()}",
                    property.ToString());
        }

        public void GetDeliveryProperties(PropertyInfo pi, OrderData orderData,
            Dictionary<string, string> formProperties)
        {
            if (orderData == null) return;
            if (pi.PropertyType != typeof(Delivery)) return;

            var delivery = (Delivery)pi.GetValue(orderData.Buyer);
            if (delivery == null) return;

            foreach (var propertyInfo in delivery.GetType().GetProperties())
            {
                var value = propertyInfo.GetValue(delivery);
                AddToFormProperties(formProperties, value, propertyInfo, "buyer.delivery");
            }
        }

        public void GetInvoiceProperties(PropertyInfo pi, OrderData orderData, Dictionary<string, string> formProperties)
        {
            if (orderData == null) return;
            if (pi.PropertyType != typeof(Invoice)) return;

            var invoice = (Invoice)pi.GetValue(orderData.Buyer);
            if (invoice == null) return;

            foreach (var propertyInfo in invoice.GetType().GetProperties())
            {
                var value = propertyInfo.GetValue(invoice);
                AddToFormProperties(formProperties, value, propertyInfo, "buyer.invoice");
            }
        }

        public void GetShippingMethodProperties(PropertyInfo pi, OrderData orderData,
            Dictionary<string, string> formProperties)
        {
            if (orderData == null) return;
            if (pi.PropertyType != typeof(List<ShippingMethod>)) return;

            var shippingMethods = (List<ShippingMethod>)pi.GetValue(orderData);
            if (shippingMethods == null) return;

            for (var i = 0; i < shippingMethods.Count; i++)
            {
                foreach (var propertyInfo in shippingMethods[i].GetType().GetProperties())
                {
                    var value = propertyInfo.GetValue(shippingMethods[i]);
                    AddToFormProperties(formProperties, value, propertyInfo, $"shippingMethods[{i}]");
                }
            }
        }

        public void GetProductProperties(PropertyInfo pi, OrderData orderData, Dictionary<string, string> formProperties)
        {
            if (orderData == null) return;
            if (pi.PropertyType != typeof(List<Product>)) return;

            var products = (List<Product>)pi.GetValue(orderData);
            if (products == null) return;

            for (var i = 0; i < products.Count; i++)
            {
                foreach (var propertyInfo in products[i].GetType().GetProperties())
                {
                    var value = propertyInfo.GetValue(products[i]);
                    AddToFormProperties(formProperties, value, propertyInfo, $"products[{i}]");
                }
            }
        }

        public string GetTotalAmount(List<Product> products)
        {
            var sum = products.Sum(product => product.Quantity * product.UnitPrice);
            return sum.ToString(CultureInfo.InvariantCulture);
        }
    }
}