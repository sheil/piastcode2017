﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dogadane.ViewModels
{
    public class UserOffersViewModel
    {
        public List<OfferItemViewModel> OpenOffers { get; set; }
        public List<OfferItemViewModel> BookedOffers { get; set; }
        public List<OfferItemViewModel> ClosedOffers { get; set; }
    }
}