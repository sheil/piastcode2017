﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dogadane.ViewModels
{
    public class RateViewModel
    {
        public int OfferId { get; set; }
        public string TargetUserId { get; set; }
        public int Contact { get; set; }
        public int Reliability { get; set; }
        public int Skill { get; set; }
    }
}