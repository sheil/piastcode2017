﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dogadane.ViewModels
{
    public class SkillAll
    {
        public int SkillId { get; set; }
        public string Name { get; set; }
    }
}