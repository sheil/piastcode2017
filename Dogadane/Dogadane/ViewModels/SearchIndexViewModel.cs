﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dogadane.Models;

namespace Dogadane.ViewModels
{
    public class SearchIndexViewModel
    {
        public List<Category> CategoryOptions { get; set; }
    }
}