﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dogadane.ViewModels
{
    public class AddOfferViewModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int CategoryId { get; set; }
    }
}