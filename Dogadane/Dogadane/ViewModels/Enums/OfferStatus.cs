﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dogadane.ViewModels.Enums
{
    public enum OfferStatus
    {
        Open = 1,
        Booked = 2,
        Closed = 3
    }
}