﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Dogadane.Startup))]
namespace Dogadane
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
