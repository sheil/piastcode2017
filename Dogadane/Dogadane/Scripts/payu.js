﻿$(document).ready(function() {
    $("#signatureSubmit").on("click", function (e) {
        e.preventDefault();
        var dataToPost = $("#payu-payment-form").serialize();

        $.post("/Payu/GetSignature", dataToPost)
            .done((response, status, jqxhr) => {
                if (response !== "Invalid") {
                    $("#signature").val(response);
                    $("#payu-payment-form").submit();
                }
            })
            .fail((jqxhr, status, error) => {
            });
    });
});
