﻿using Dogadane.Models.Payu;

namespace Dogadane.Interfaces
{
    public interface IOrderService
    {
        string CreateSignature(OrderData orderData, string clientKey);
    }
}
