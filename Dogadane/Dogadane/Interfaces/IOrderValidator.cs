﻿using Dogadane.Models.Payu;

namespace Dogadane.Interfaces
{
    public interface IOrderValidator
    {
        bool IsOrderDataValid(OrderData orderData);
    }
}
