﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Dogadane.Utils
{
    public static class Helpers
    {
        public static string FirstLetterToLower(this string text)
        {
            if (text != String.Empty && char.IsUpper(text[0]))
            {
                text = char.ToLower(text[0]) + text.Substring(1);
            }
            return text;
        }

        public static bool IsNullOrEmpty(object value)
        {
            if (ReferenceEquals(value, null))
                return true;

            var type = value.GetType();

            if (type == typeof(string))
            {
                return string.IsNullOrEmpty(value.ToString());
            }

            return type.IsValueType
                && Equals(value, Activator.CreateInstance(type));
        }

        public static string ReplaceUtf8LowercaseWithUppercase(string text)
        {
            var regex = new Regex(@"%[a-f0-9]{2}");

            var urlEncodedUppercase = regex.Replace(text, m => m.Value.ToUpperInvariant());
            return urlEncodedUppercase;
        }

        public static string CreateAndHashOrderSignature(string signature, string merchantPosId, string algorithm)
        {
            if (string.IsNullOrEmpty(signature)) return String.Empty;

            var hashedSignature = ShaAlgorithm.GetHashSha256(signature);
            return $"sender={merchantPosId};algorithm={algorithm};signature={hashedSignature}";
        }


        public static string ConcatenateSignature(string clientKey, List<KeyValuePair<string, string>> sortedFormProperties)
        {
            var sb = new StringBuilder();
            foreach (var property in sortedFormProperties)
            {
                sb.Append($"{property.Key}={HttpUtility.UrlEncode(property.Value)}");
                sb.Append($"&");
            }

            sb.Append(clientKey);
            return sb.ToString();
        }
    }
}
