﻿using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Dogadane.Utils
{
    public class ShaAlgorithm
    {
        public static string GetHashSha256(string text)
        {
            var bytes = Encoding.UTF8.GetBytes(text);
            var hashstring = new SHA256Managed();
            var hash = hashstring.ComputeHash(bytes);
            return hash.Aggregate(string.Empty, (current, x) => current + $"{x:x2}");
        }
    }
}