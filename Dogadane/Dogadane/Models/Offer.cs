﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dogadane.ViewModels.Enums;

namespace Dogadane.Models
{
    public class Offer
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime DateAdded { get; set; }
        public OfferStatus Status { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }

        public string SourceUserId { get; set; }
        public virtual ApplicationUser SourceUser { get; set; }

        public string AssociatedUserId { get; set; } //ktos kto chce sie dogadac :D
        public virtual ApplicationUser AssociatedUser { get; set; }
    }
}