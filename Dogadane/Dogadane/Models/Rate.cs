﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dogadane.Models
{
    public class Rate
    {
        public int Id { get; set; }
        public int Contact { get; set; }
        public int Reliability { get; set; }
        public int Skill { get; set; }

        public int OfferId { get; set; }
        public virtual Offer Offer { get; set; }

        public string SourceUserId { get; set; }
        public virtual ApplicationUser SourceUser { get; set; }

        public string TargetUserId { get; set; }
        public virtual ApplicationUser TargetUser { get; set; }
    }
}