﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dogadane.Models
{
    public class Skill
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual List<ApplicationUser> User { get; set; }
    }
}