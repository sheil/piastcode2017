﻿using System.Collections.Generic;

namespace Dogadane.Models.Payu
{
    public class OrderData
    {
        public string AdditionalDescription { get; set; }
        public string CurrencyCode { get; set; }
        public string CustomerIp { get; set; }
        public string Description { get; set; }
        public int ExtOrderId { get; set; }
        public string MerchantPosId { get; set; }
        public string NotifyUrl { get; set; }
        public string ContinueUrl { get; set; }
        public string OrderUrl { get; set; }
        public List<Product> Products { get; set; }
        public Buyer Buyer { get; set; }
        public List<ShippingMethod> ShippingMethods { get; set; }
        public string Signature { get; set; }
        public string TotalAmount { get; set; }
        public int Selected { get; set; }
    }
}