﻿using System;

namespace Dogadane.Models.Payu
{
    public class Product
    {
        public string Name { get; set; }
        public int Quantity { get; set; }
        public int UnitPrice { get; set; }
        public bool Virtual { get; set; }
        public DateTime ListingDate { get; set; }
    }
}