﻿namespace Dogadane.Models.Payu
{
    public class ShippingMethod
    {
        public string Country { get; set; }
        public string Name { get; set; }
        public string Price { get; set; }
    }
}