﻿namespace Dogadane.Models.Payu
{
    public class Delivery
    {
        public string City { get; set; }
        public string CountryCode { get; set; }
        public string Name { get; set; }
        public string PostalCode { get; set; }
        public string RecipientEmail { get; set; }
        public string RecipientName { get; set; }
        public string RecipientPhone { get; set; }
        public string Street { get; set; }
    }
}