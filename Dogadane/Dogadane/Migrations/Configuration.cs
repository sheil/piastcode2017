using System.Collections.Generic;
using Dogadane.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Dogadane.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Dogadane.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(Dogadane.Models.ApplicationDbContext context)
        {
            context.Categories.AddRange(new List<Category>
            {
                new Category() { Name = "grafika" },
                new Category() { Name = "medycyna" },
                new Category() { Name = "programowanie" },
                new Category() { Name = "architekt" },
                new Category() { Name = "ekonomia" },
                new Category() { Name = "prawo" },
                new Category() { Name = "in�ynieria �rodowiska" },
                new Category() { Name = "tworzenie stron internetowych" },
                new Category() { Name = "automatyka" },
                new Category() { Name = "pedagogia" },
                new Category() { Name = "farmacja" },
                new Category() { Name = "ksi�gowo��" },
                new Category() { Name = "geodezja" },
                new Category() { Name = "trener personalny" },
                new Category() { Name = "dziennikarstwo" },
                new Category() { Name = "handel" },
                new Category() { Name = "windykacja" },
                new Category() { Name = "�lusarstwo" },
                new Category() { Name = "malowanie" },
                new Category() { Name = "drobne naprawy" },
                new Category() { Name = "nauczyciel" },
                new Category() { Name = "j�zyk angielski" },
                new Category() { Name = "j�zyk niemiecki" },
                new Category() { Name = "j�zyk francuski" },
                new Category() { Name = "j�zyk polski" },
                new Category() { Name = "matematyka" },
                new Category() { Name = "fizyka" },
                new Category() { Name = "informatyka" }
            });

            if (!context.Users.Any(u => u.UserName == "testuser"))
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                var user = new ApplicationUser { UserName = "testuser@dogadane.pl", Email = "testuser@dogadane.pl"};

                manager.Create(user, "password");
            }
        }
    }
}
